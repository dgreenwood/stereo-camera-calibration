import numpy as np
import pickle
import cv2
import logging
import json

from .utils import solvePnP, homogenise, dehomogenise, projection_matrix
from .utils import object_points, find_corners, ChessboardNotFoundError

logger = logging.getLogger(__name__)

CALIB_FLAGS = cv2.CALIB_FIX_ASPECT_RATIO + \
    cv2.CALIB_FIX_PRINCIPAL_POINT + \
    cv2.CALIB_ZERO_TANGENT_DIST
CRITERIA = (cv2.TERM_CRITERIA_MAX_ITER +
            cv2.TERM_CRITERIA_EPS, 30, 1e-6)

BRD_SIZE = (3, 7)  # or (5, 9)
IMG_SIZE = (720, 1280)
SQR_SIZE = 0.020  # or 0.015 metres


# -----------------------------------------------------------------------------
# module functions
# -----------------------------------------------------------------------------


def calibration_to_dict(calib, obj_pts=None, img_pts=None, img_size=IMG_SIZE):
    """
    Convert the calibration returned by cv2.calibrateCamera, along with
    the object points, image points, and image size.
    Board size and square size can be derived from object points.
    """
    def _array(x):
        return np.array(x).astype('float32').squeeze()
    d = dict()
    d['rms'] = _array(calib[0])
    d['mtx'] = _array(calib[1])
    d['dist'] = _array(calib[2])
    d['rvecs'] = _array(calib[3])
    d['tvecs'] = _array(calib[4])
    d['obj_pts'] = _array(obj_pts)
    d['img_pts'] = _array(img_pts)
    d['img_size'] = tuple(img_size)
    return d


def calibrate_camera(obj_pts, img_pts, img_size=IMG_SIZE,
                     flags=CALIB_FLAGS, criteria=CRITERIA):
    calib = cv2.calibrateCamera(
        obj_pts, img_pts, img_size, np.eye(3),
        np.zeros(5), flags=flags, criteria=criteria)
    return calibration_to_dict(calib, obj_pts, img_pts, img_size)


def save(calib, fname):
    """
    Save the calibration (which is a dictionary) to fname as pkl.
    There appear to be some compatability issues with pickle and
    python 2 to 3, so, convert all the nd arrays to lists before
    writing. Of course, this will need to be reversed when loading.
    """
    d = dict()
    for k in calib:
        if not isinstance(k, np.ndarray):
            d[k] = calib[k]
            continue
        d[k] = calib[k].tolist()
    with open(fname, 'wb') as fid:
        pickle.dump(d, fid, protocol=2)
    logger.info('Saving file at: {}'.format(fname))


def load(fname):
    """ Load a calibration from a pickle file, return as dictionary"""
    def _array(x):
        return np.array(x).astype('float32').squeeze()

    with open(fname, 'rb') as fid:
        d = pickle.load(fid)

    calib = dict()
    for k in d:
        if k == 'img_size':
            calib[k] = tuple(d[k])
            continue
        calib[k] = _array(d[k])
    return calib
