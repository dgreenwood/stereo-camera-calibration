import numpy as np
import cv2
import logging

from scipy.optimize import least_squares

from .utils import solvePnP, get_transform, procrustes, projection_matrix
from .calibrate import min_calib, get_intrinsics, project_targets
from .stereo_calibrate import calibrate_pair


logger = logging.getLogger(__name__)


# -----------------------------------------------------------------------------
# Module functions
# -----------------------------------------------------------------------------


def calibrate_procrustes(calibs):
    """
    By projecting all the targets for each camera, get the transform between
    each camera using procrustes. Three cameras only, so return a
    single concatenated list of [camera 0 parameters] +
    [r t between camera 0 and camera 1] + [camera 1 parameters] ...
    and so on for each camera, such that the list has an r,t between
    each camera.
    camera params are 4 floats,
    r has 3 floats (which is  a rotation vector), and
    t has 3 floats - a translation vector.

    calibs is a list of camera calibration dictionaries.
    """

    t0 = project_targets(calibs[0]).reshape(-1, 3)
    t1 = project_targets(calibs[1]).reshape(-1, 3)
    t2 = project_targets(calibs[2]).reshape(-1, 3)

    result = min_calib(calibs[0])
    for i, (a, b) in enumerate([(t0, t1), (t0, t2)]):
        r, t = procrustes(a, b)[2:]
        result += [float(x) for x in cv2.Rodrigues(r)[0]]
        result += [float(x) for x in t]
        result += min_calib(calibs[i+1])
    return np.asfarray(result)


def calibrate_pairs(calibs):
    """ Calibrate the transform between each camera.
    return a single concatenated list of [camera 0 parameters] +
    [r t between camera 0 and camera 1] + [camera 1 parameters] ...
    and so on for each camera, such that the list has an r,t between
    each camera.
    camera params are 4 floats,
    r has 3 floats (which is  a rotation vector), and
    t has 3 floats - a translation vector.

    calibs is a list of camera calibration dictionaries.
    """
    n = len(calibs)
    result = min_calib(calibs[0])
    for i in range(1, n):
        r, t, = calibrate_pair(calibs[0], calibs[i])[:2]
        result += [float(x) for x in cv2.Rodrigues(r)[0]]
        result += [float(x) for x in t]
        result += min_calib(calibs[i])
    return np.asfarray(result)


def compute_distance(left, right, M, ops, left_pts, right_pts,
                     l2=True, scale=1e3):
    """
    Compute the distance between the 3d target points observed by two cameras.
    left, right are the mini calibrations of each camera, see:
        calibrate.min_calib(calib)

    M is the tranformation matrix, such that the right points are transformed
    in world space to lie (closely) on the left points. see:
        get_transform(r, t, inv=True)

    ops are the obj_points from the calibrations
    left_pts, right_pts  are the image points from the calibrations.

    l2 = True is squared error, else abs error.
    scale = 1000 to show error as millimeters if board dims is in meters.
    """
    op_augmented = np.concatenate([ops[0].T, np.ones([1, len(ops[0])])])
    diff = []
    F = np.square if l2 else np.abs

    for (lip, rip) in zip(left_pts, right_pts):

        L = solvePnP(ops[0], lip, *get_intrinsics(left))
        R = solvePnP(ops[0], rip, *get_intrinsics(right))

        left_p = L.dot(op_augmented)
        right_p = M.dot(R.dot(op_augmented))
        diff.append(F(left_p - right_p).sum(axis=0))

    result = np.concatenate(diff).ravel()
    return result


def get_initial_values(calibs):
    """
    Return initial values from a list of calibrations for bundle adjustment.
    """
    params = calibrate_pairs(calibs)
    obj_pts = [c['obj_pts'] for c in calibs]
    img_pts = [c['img_pts'] for c in calibs]
    return params, obj_pts, img_pts


def fun(params, obj_pts, img_pts):
    n, op, residuals = len(img_pts), obj_pts[0], []
    cams = [params[i*10:4+i*10] for i in range(n)]
    rs = [params[4+i*10:7+i*10] for i in range(n-1)]
    ts = [params[7+i*10:10+i*10] for i in range(n-1)]

    # between each pair
    for i in range(1, n):
        left, lip = cams[i-1], img_pts[i - 1]
        right, rip = cams[i], img_pts[i]
        M = get_transform(rs[i-1], ts[i-1], inv=True)
        dist = compute_distance(left, right, M, op, lip, rip)
        residuals.append(dist)

    # first to last
    left, right = cams[0], cams[-1]
    lip, rip, = img_pts[0], img_pts[-1]
    M1 = get_transform(rs[0], ts[0])
    M2 = get_transform(rs[-1], ts[-1])
    M = np.linalg.inv(M2.dot(M1))
    dist = compute_distance(left, right, M, op, lip, rip)
    residuals.append(dist)
    return np.asfarray(residuals).ravel()


def ranked_idx(residuals, shape, trim=5):
    """
    residuals as returned from least squares
    shape = [num_cams, num_obs, -1]
    trim: how many worst observations to trim.
    """

    max_errors = residuals.reshape(shape).max(-1).max(0)
    ranked_x = sorted([v, i] for i, v in enumerate(max_errors))
    return np.array([int(i[1]) for i in ranked_x[:-trim]], dtype='int32')


def optimise(calibs, iters=4, fun=fun):
    """
    Optimise the camera parameters as a bundle.
    """
    x0, obj_pts, img_pts = get_initial_values(calibs)
    xt, fs = 1e-6, 1e1
    mean_error = fun(x0, obj_pts, img_pts).mean()
    logger.info('Intial mean error: {}'.format(mean_error))
    for i in range(iters):
        res = least_squares(fun, x0, method='trf', loss='soft_l1',
                            xtol=xt, f_scale=fs,
                            args=(obj_pts, img_pts))
        # adjust values for next iter - we remove the worst calibrations
        logger.info('Mean error {}: {}'.format(i + 1, res.fun.mean()))
        shape = [len(calibs), len(obj_pts[0]), -1]
        idx = ranked_idx(res.fun, shape)
        xt, fs = xt*0.1, fs*0.1
        obj_pts = [op[idx] for op in obj_pts]
        img_pts = [ip[idx] for ip in img_pts]
    return res.x


def get_calibs(params):
    """
    Convert the list of parameters returned from optimise.
    This is only for three cameras with two transforms.
    returns a list of dictionaries. Each dictionary has
    'mtx', 'dist', 'R', 't', and 'P'.
    """
    j, cameras = 1, []
    for i in range(0, 24, 10):
        m, d = get_intrinsics(params[i:i+4])
        cameras.append({'mtx': m, 'dist': d})

    cameras[0]['R'] = np.eye(3)
    cameras[0]['t'] = np.zeros(3)
    cameras[0]['P'] = projection_matrix(cameras[0]['mtx'])

    for i in range(4, 24, 10):
        r, t = params[i:i+3], params[i+3:i+6]
        R = cv2.Rodrigues(r)[0]
        cameras[j]['R']=R
        cameras[j]['t']=t
        cameras[j]['P']=projection_matrix(cameras[j]['mtx'], R, t)
        j+=1
    return cameras
