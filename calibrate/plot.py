import matplotlib.pyplot as plt
import numpy as np
import cv2

from .utils import object_points

"""
Some utilities for plotting camera calibrations:

Example:
import matplotlib.pyplot as plt
fig = plt.figure(figsize=[10,10])
ax = fig.add_subplot(111, projection='3d')
target(ax, pts, shape=[9, 5])

"""


def ax_equal(ax):
    """Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    """
    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])


def ax_label(ax):
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')


def set_lim(ax):
    """ get the max axes lim and set all the same """
    x = max(abs(i) for i in ax.get_xlim())
    y = max(abs(i) for i in ax.get_ylim())
    lim = max(x, y)
    ax.set_xlim([-lim, lim])
    ax.set_ylim([-lim, lim])


def repro_error(ax, reprojection_errors):
    """ plot the reprojection error of a camera calibration.
    reprojection_errors is an array of shape: n_targets*target_size*2
    """
    for p in reprojection_errors:
        ax.scatter(p.T[0], p.T[1], marker='x')
    ax.set_aspect('equal')
    ax.set_xlabel('X Error (px)')
    ax.set_ylabel('Y Error (px)')
    ax.set_title('Reprojection Error')


def distortion(ax, mtx, dist,
                    n_pts=20,
                    sqr_size=0.02, z=2.0,
                    img_size=(720, 1280)):
    """
    Plot a grid of points to illustrate camera distortion.
    mtx: a 3*3 camera intrinsic matrix.
    dist: distortion [k1,k2,k3, .. k5]
    n_pts: the grid n_pts*n_pts
    sqr_size: in metres
    z: the z position of the 'board' in metres.
    img_size: size of the image in pixels.
    """

    shift = np.array([(n_pts // 2) * sqr_size, (n_pts // 2) * sqr_size, 0])
    obj = object_points(1 + n_pts, 1 + n_pts, sqr_size) - shift
    rvec, tvec, xy = np.zeros(3), np.array([0, 0, float(z)]), mtx[:2, 2]

    # Project the points
    pts, _ = cv2.projectPoints(obj, rvec, tvec, mtx, np.zeros(5))
    pts = pts.squeeze() - xy
    dist_pts, _ = cv2.projectPoints(obj, rvec, tvec, mtx, dist)
    dist_pts = dist_pts.squeeze() - xy

    ax.plot(*pts.T, marker='x', label='pattern', lw=0)
    ax.plot(*dist_pts.T, marker='.', label='distorted', lw=0)

    if img_size is not None:
        img = np.array([[-0.5, -0.5], [-0.5, 0.5], [0.5, 0.5],
                        [0.5, -0.5], [-0.5, -0.5]]) * np.array(img_size)
        ax.plot(*img.T, label='image limit')

    ax.axis('equal')
    ax.set_xlabel('x (pixels)')
    ax.set_ylabel('y (pixels)')
    ax.set_title('Projected Distortion')
    ax.grid()
    ax.legend()


def two(ax, left, right, lines=False):
    """
    plot two images side by side.

    if lines == True, plot horizontal  lines over each image.
    this is to check correspondence for stereo rectified images.
    """
    ax[0].imshow(left, cmap='gray')
    ax[1].imshow(right, cmap='gray')
    if lines:
        [ylim, xlim], dist = left.shape, 100
        ax[0].autoscale(False)
        ax[1].autoscale(False)
        for y in range(dist, ylim, dist):
            ax[0].plot([0, xlim], [y, y], 'r', lw=2.0)
            ax[1].plot([0, xlim], [y, y], 'r', lw=2.0)


def target(ax, pts, shape=[9, 5], **kwargs):
    """ draw an opencv chessboard target"""
    [m, n], x, y, z = shape, [], [], []
    for i in range(n):
        j = i * m
        x += [pts[j][0], pts[j + m - 1][0], np.nan]
        y += [pts[j][1], pts[j + m - 1][1], np.nan]
        z += [pts[j][2], pts[j + m - 1][2], np.nan]
    for i in range(m):
        k = (n-1) * m
        x += [pts[i][0], pts[i + k][0], np.nan]
        y += [pts[i][1], pts[i + k][1], np.nan]
        z += [pts[i][2], pts[i + k][2], np.nan]
    ax.plot(x, y, z, **kwargs)


def camera(ax, R=None, t=None, name=None,
           size=1.0, z_size=1.0, cols=['r', 'g', 'b', 'gray']):
    """
    Draw a camera at the origin, or, translate it using
    a 3x3 rotation matrix (R) and translation vector (t).
    """
    eps = 0.001
    scale = max(eps, size)
    z_len = max(eps, z_size)
    cam = np.array([[0, 0, 0], [1, 0, 0],
                    [0, 1, 0],  [0, 0, z_len]]) * scale
    fst = np.array([[0, 0, 0], [-1, -1, 2], [-1, 1, 2],
                    [1, 1, 2], [1, -1, 2]]) * 0.5 * scale

    if R is None:
        R = np.eye(3)
    if t is None:
        t = np.zeros(3)

    cam = np.dot(R, cam.T).T + t.ravel()
    fst = np.dot(R, fst.T).T + t.ravel()

    # cam
    ax.plot(*zip(cam[0], cam[1]), color=cols[0])
    ax.plot(*zip(cam[0], cam[2]), color=cols[1])
    ax.plot(*zip(cam[0], cam[3]), color=cols[2])

    if name is not None:
        x, y, z = cam[0]
        ax.text(x, y, z, name, fontsize=12)

    # frustum
    ax.plot(*zip(fst[0], fst[1]), color=cols[3])
    ax.plot(*zip(fst[0], fst[2]), color=cols[3])
    ax.plot(*zip(fst[0], fst[3]), color=cols[3])
    ax.plot(*zip(fst[0], fst[4]), color=cols[3])
    ax.plot(*zip(fst[1], fst[2], fst[3], fst[4], fst[1]), color=cols[3])
