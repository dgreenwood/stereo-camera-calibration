import numpy as np
import json
import cv2
import logging
from scipy.sparse import lil_matrix

from .utils import projection_matrix, rodrigues, triangulate_npoints
from .utils import homogenise, dehomogenise, undistort_points, object_points
from .utils import procrustes, invRt, getM, getRt
from .plot import ax_label, ax_equal, camera, repro_error, distortion
from .calibrate import CALIB_FLAGS, CRITERIA

logger = logging.getLogger(__name__)

# -----------------------------------------------------------------------------
# ENUMS - flags
# -----------------------------------------------------------------------------

CAM_FOCAL = 1
CAM_DIST = 2
CAM_RT = 4

# -----------------------------------------------------------------------------
# Static Methods
# -----------------------------------------------------------------------------


def convert_flags(flags):
    """ Convert a flag (an int 1-7) via a bit pattern, to array indices.
    eg: 5 -> '101' -> [0, 2]
    """
    assert 0 < flags < 8
    s = reversed('{:03b}'.format(flags))
    return [j for j, x in enumerate(s) if int(x) == 1]


def stereo_calibrate(A, B, flags=None, criteria=None):
    """Calculate the transform between two cameras"""
    if flags is None:
        flags = cv2.CALIB_FIX_INTRINSIC
    if criteria is None:
        criteria = (cv2.TERM_CRITERIA_MAX_ITER +
                    cv2.TERM_CRITERIA_EPS, 30, 1e-6)
    [err, Ma, da, Mb, db, R, t, _, _] = cv2.stereoCalibrate(
        A.obj_pts.astype(np.float32),
        A.img_pts.astype(np.float32),
        B.img_pts.astype(np.float32),
        A.mtx.astype(np.float32),
        A.dist.astype(np.float32),
        B.mtx.astype(np.float32),
        B.dist.astype(np.float32),
        tuple(A.img_size), flags=flags, criteria=criteria)
    logger.info('Stereo error: {:0.3f}'.format(err))
    return Ma, da, Mb, db, R, t


# -----------------------------------------------------------------------------
# Camera class
# -----------------------------------------------------------------------------

class Camera(object):
    """Class that stores the camera data"""

    def __init__(self, params=None, name=None):
        super(Camera, self).__init__()
        self._defaults()
        if params is not None:
            self.from_dict(params)
        self.name = name
        self.f = self.mtx[0, 0]

    def _defaults(self):
        """Set default values."""
        self.mtx = np.eye(3)
        self.dist = np.zeros(5)
        self.R = np.eye(3)
        self.t = np.zeros([3, 1])
        self.obj_pts = np.array([])
        self.img_pts = np.array([])
        self.rvecs = np.array([])
        self.tvecs = np.array([])
        self.img_size = (720, 1280)

    def string(self, Rt=True):
        s = '{}\n'.format(self.name) + \
            'mtx :  {:7.2f}, {:7.2f}, {:7.2f}\n'.format(*self.mtx[0, :]) + \
            '       {:7.2f}, {:7.2f}, {:7.2f}\n'.format(*self.mtx[1, :]) + \
            '       {:7.2f}, {:7.2f}, {:7.2f}\n'.format(*self.mtx[2, :]) + \
            '\ndist : ' + \
            ', '.join('{:7.2f}'.format(i) for i in self.dist.ravel())
        if Rt:
            s += '\n\n' + \
                'R :    {:7.2f}, {:7.2f}, {:7.2f}\n'.format(*self.R[0, :]) + \
                '       {:7.2f}, {:7.2f}, {:7.2f}\n'.format(*self.R[1, :]) + \
                '       {:7.2f}, {:7.2f}, {:7.2f}\n'.format(*self.R[2, :]) + \
                '\n' + \
                't :    {:7.2f}, {:7.2f}, {:7.2f}'.format(*self.t.ravel())
        return s + '\n'

    def set_scale(self):
        """Set the scaling vector"""
        scale = [1e3, 1., 1., 1.]
        self.scale = np.array(scale)
        return self.scale

    def params(self):
        """Return the parameters as 1d ndarray, in order:
        focal length, f, (1 float)
        radial distortion params (3 floats)
        world rotation vector (3 floats)
        world translation vector (3 floats)
        all the rvevs and tvecs (n_targets * 6)
        """
        cams = self.min_cal() + self.min_Rt()
        cams[:4] /= self.set_scale()
        rtvecs = np.hstack([self.rvecs, self.tvecs]).flatten()
        params = np.concatenate([cams, rtvecs])
        return params

    def load_params(self, params):
        """
        Inverse of params() - load the camera parameters from a list of values.
        """
        params = np.asfarray(params).copy()
        params[:4] *= self.scale
        rtvecs = np.array(params[10:]).reshape(-1, 6)
        self.load_min(params[0], params[1:4], params[4:7], params[7:10])
        self.rvecs = rtvecs[:, :3]
        self.tvecs = rtvecs[:, 3:]

    def P(self):
        """Return the projection matrix"""
        return projection_matrix(self.mtx, self.R, self.t)

    def from_dict(self, params):
        """Load data from dictionary. All lists are converted to ndarray."""
        for key in params:
            self.__dict__[key] = np.asarray(params[key], dtype=np.float32) \
                if type(params[key]) == list else params[key]

    def to_dict(self):
        """Convert to dictionary without numpy arrays """
        d = dict()
        for key, value in self.__dict__.items():
            d[key] = value.squeeze().tolist() \
                if type(value) == np.ndarray else value
        return d

    def save_json(self, fname):
        """Convert to dictionary of lists, then save to fname."""
        with open(fname, 'wb') as fid:
            json.dump(self.to_dict(), fid, indent=2)

    def min_dist(self):
        """ radial distortion as list"""
        k1, k2, _, _, k3 = self.dist
        return [k1, k2, k3]

    def min_cal(self):
        """convert calibration to list"""
        f = self.mtx[0, 0]
        k1, k2, _, _, k3 = self.dist
        return [f, k1, k2, k3]

    def min_Rt(self):
        """convert R, t to list"""
        return rodrigues(self.R).ravel().tolist() + \
            np.asfarray(self.t).ravel().tolist()

    def set_Rt(self, R, t):
        """set R and t for this cam"""
        self.R = R
        self.t = np.asfarray(t).reshape(3, 1)

    def load_min(self, f=None, dist=None, r=None, t=None):
        """convert minimal values and replace in instance"""
        if dist is not None:
            [k1, k2, k3] = dist
            self.dist = np.asfarray([k1, k2, 0, 0, k3])
        if f is not None:
            M = np.eye(3)
            M[[0, 1], [0, 1]] = f
            M[:2, 2] = np.array(self.img_size) * 0.5
            self.mtx = M.astype(np.float)
        if r is not None:
            R = rodrigues(r)
            self.R = R
        if t is not None:
            t = np.asfarray(t).reshape(3, 1)
            self.t = t

    def _rts(self, obj_pts=None, img_pts=None):
        if obj_pts is None:
            obj_pts = self.obj_pts
        if img_pts is None:
            img_pts = self.img_pts
        for ob, ip in zip(obj_pts, img_pts):
            yield cv2.solvePnP(ob, ip, self.mtx, self.dist)[1:]

    def targets_to_world(self, reshape=False):
        """ Transform the target object points to world space. """
        transformed_points = []
        wR, wt = invRt(self.R, self.t)
        for r, t in self._rts():
            R, t_obj = rodrigues(r), self.obj_pts[0].T
            t_obj = np.dot(R, t_obj) + t
            t_obj = np.dot(wR, t_obj) + wt
            transformed_points.append(t_obj.T)
        targets = np.asfarray(transformed_points, dtype=np.float32)
        return targets.reshape(-1, 3) if reshape else targets

    def project_to_image(self, obj_pts):
        """ Project 3d World points to image points."""
        r = rodrigues(self.R)
        t = self.t
        obj_pts = np.asfarray(obj_pts).astype(np.float32)
        pp = cv2.projectPoints(obj_pts, r, t, self.mtx, self.dist)[0]
        return pp.squeeze()

    def targets_to_image(self):
        """ Project all the 3d target points to image coordinates."""
        projection = []
        for op, rv, tv in zip(self.obj_pts, self.rvecs, self.tvecs):
            pp = cv2.projectPoints(op, rv, tv, self.mtx, self.dist)[0]
            projection.append(pp.squeeze())
        return np.asarray(projection).astype(np.float32)

    def undistorted_points(self, pt_array=None, homogenise=False):
        """
        Undistort the image domain points.
        pt_array is N*2 array of x,y points. If pt_array is None, the stored
        calibration points are projected.
        """
        K, dist = self.mtx, self.dist
        pt_array = self.img_pts.reshape(-1, 2) if pt_array is None \
            else np.asfarray(pt_array, dtype=np.float32).reshape(-1, 2)
        pts = undistort_points(pt_array, K, dist)
        if homogenise:
            ones = np.ones([len(pts), 1])
            pts = np.hstack([pts, ones])
        return pts.astype(np.float32)

    def projection_error(self, img_pts=None, obj_pts=None,
                         reshape=True, euclidian=True):
        """Return the error between observed and projected image points.
        If reshape is true, merge all targets.
        If euclidian is true, return the squared distance."""

        if img_pts is None:
            img_pts = self.img_pts

        if obj_pts is None:
            prj_pts = self.targets_to_image()
        else:
            img_pts = img_pts.reshape(-1, 2)
            prj_pts = self.project_to_image(obj_pts)

        error = np.asfarray([ip-pp for ip, pp in zip(img_pts, prj_pts)])

        if reshape:
            error = error.reshape(-1, 2)
        if euclidian:
            error = np.sqrt(np.square(error).sum(-1))
        return error

# -----------------------------------------------------------------------------
# plotting
# -----------------------------------------------------------------------------

    def plot_camera(self, ax, equal=False, label=False):
        """
        Plot a visualisation of the camera, in 3d.
        """
        R, t = invRt(self.R, self.t)
        camera(ax, R=R, t=t, name=self.name,
               size=1.0, z_size=2.5,
               cols=['r', 'g', 'b', 'gray'])
        if equal:
            ax_equal(ax)
        if label:
            ax_label(ax)

    def plot_repro_error(self, ax):
        """
        Plot the reprojection error of a camera calibration.
        """
        pts = self.projection_error(reshape=False, euclidian=False)
        repro_error(ax, pts)

    def plot_distortion(self, ax, n_pts=20, sqr_size=0.02, z=2.0,
                        show_img=True):
        """
        Plot a projection of 40x40 points to illustrate the distortion.
        """
        img_size = self.img_size if show_img else None
        distortion(ax, self.mtx, self.dist, n_pts, sqr_size, z, img_size)

    def plot_projection(self, ax):
        """
        Project all the 3d points to 2d image space and plot them ('.' marker).
        Also plot the observed image points for comparison ('x' marker).
        """
        pts = self.targets_to_image()
        for p, ip in zip(pts, self.img_pts):
            ax.scatter(*ip.T, c='r', marker='x')
            ax.scatter(*p.T, c='g', marker='.')
        ax.scatter([], [], c='r', marker='x', label='image pts')
        ax.scatter([], [], c='g', marker='.', label='projected pts')
        ax.axis('equal')
        ax.set_xlabel('x (pixels)')
        ax.set_ylabel('y (pixels)')
        ax.set_title('Projected Points')
        ax.legend()

# -----------------------------------------------------------------------------
# Three cameras
# -----------------------------------------------------------------------------


class ThreeCameras(object):
    """
    Bundle adjust three cameras - left, centre, right
    R, t of left camera transforms centre to left. R, t of right
    camera tranforms centre to right. This allows the transformed
    cameras to project their respective calibrations on top of each other.
    """

    def __init__(self, camera_list, flags=7, transform=True):
        super(ThreeCameras, self).__init__()
        self.flags = flags
        self.left = camera_list[0]
        self.centre = camera_list[1]
        self.right = camera_list[2]
        self.left.name = 'left'
        self.right.name = 'right'
        self.centre.name = 'centre'
        self.centre_loss = False
        self.cams = [self.left, self.centre, self.right]
        if transform:
            self.transform()

    def string(self):
        """return a string of class data"""
        s = ''.join(['-'] * 50) + '\n' + \
            '\n'.join(cam.string() for cam in self.cams) + '\n' + \
            ''.join(['-'] * 50) + '\n'
        return s

    def transform(self):
        """
        Compute the transform between each camera. Using Procrustes.
        """
        left = self.left.targets_to_world(reshape=True)
        right = self.right.targets_to_world(reshape=True)
        centre = self.centre.targets_to_world(reshape=True)
        d, _, lR, lt = procrustes(centre, left)
        logger.info('left error: {:0.3f}'.format(d))
        d, _, rR, rt = procrustes(centre, right)
        logger.info('right error: {:0.3f}'.format(d))
        self.left.set_Rt(lR, lt)
        self.right.set_Rt(rR, rt)

    def params(self):
        """
        Return the minimal parameters as a float array.
        Stack all the camera parameters first, then all the Rts.
        Remove the R, t params from the centre camera, so the total camera
        parameters is 24.
        """
        cam = np.concatenate([self.left.params()[:10],
                              self.centre.params()[:4],
                              self.right.params()[:10]])
        rts = np.concatenate([c.params()[10:] for c in self.cams])
        return np.concatenate([cam, rts])

    def load_params(self, params):
        """
        Expand the calibration from the minimal list representation.
        The center camera is held at zero R, t.
        """
        rts = params[24:]
        n = len(rts) // 3
        left = params[:10]
        centre = np.concatenate([params[10:14], np.zeros(6)])
        right = params[14:24]
        for i, (cp, cam) in enumerate(zip([left, centre, right], self.cams)):
            cparam = np.concatenate([cp, rts[i*n:i*n+n]])
            cam.load_params(cparam)

    def mean_targets(self):
        """ Transform the target object points to world space with the
        mean transform of each camera. """
        transformed_points = []
        l, c, r = self.cams
        lwM = getM(l.R, l.t, inv=True)
        cwM = getM(c.R, c.t, inv=True)
        rwM = getM(r.R, r.t, inv=True)
        for (lrt, crt, rrt) in zip(l._rts(), c._rts(), r._rts()):
            lM = lwM.dot(getM(*lrt))
            cM = cwM.dot(getM(*crt))
            rM = rwM.dot(getM(*rrt))
            R, t = getRt((lM + cM + rM) / 3)
            t_obj = self.centre.obj_pts[0].T
            t_obj = np.dot(R, t_obj) + t
            transformed_points.append(t_obj.T)
        targets = np.asfarray(transformed_points, dtype=np.float32)
        return targets.reshape(-1, 3).copy()

    def triangulate_left(self, lc_img_pts=None):
        if lc_img_pts is None:
            ip = [c.undistorted_points(homogenise=True) for c in self.cams[:2]]
        else:
            ip = [homogenise(x) for x in lc_img_pts]
        P = [self.left.P(), self.centre.P()]
        return triangulate_npoints(P, ip)[:, :3]

    def triangulate_right(self, cr_img_pts=None):
        """The stereo projection of the target points. """
        if cr_img_pts is None:
            ip = [c.undistorted_points(homogenise=True) for c in self.cams[1:]]
        else:
            ip = [homogenise(x) for x in cr_img_pts]
        P = [self.centre.P(), self.right.P()]
        return triangulate_npoints(P, ip)[:, :3]

    def triangulate_points(self, lcr_img_pts=None):
        """The stereo projection of the target points. """
        if lcr_img_pts is None:
            ip = [c.undistorted_points(homogenise=True) for c in self.cams]
        else:
            ip = [homogenise(x) for x in lcr_img_pts]
        P = [self.left.P(), self.centre.P(), self.right.P()]
        return triangulate_npoints(P, ip)[:, :3]

    def target_error(self):
        """The euclidian error between each target projection"""
        diffs = [self.centre.targets_to_world(reshape=True) -
                 c.targets_to_world(reshape=True)
                 for c in [self.left, self.right]]
        diff = np.concatenate(
            [np.square(diff).sum(-1) for diff in diffs])
        return diff


# -----------------------------------------------------------------------------
# bundle adjustment
# -----------------------------------------------------------------------------

    def _centre_loss(self):
        """
        Return the residual errors. For each camera, the euclidian
        re-projection error.
        """
        obj_pts = self.triangulate_points().copy()
        left = self.left.projection_error(obj_pts=obj_pts)
        centre = self.centre.projection_error(obj_pts=obj_pts)
        right = self.right.projection_error(obj_pts=obj_pts)
        residuals = np.concatenate([left, centre, right])
        return residuals

    def _left_right_loss(self):
        """
        Return the residual errors. For each camera, the euclidian
        re-projection error.
        """
        left_pts = self.triangulate_left().copy()
        ll = self.left.projection_error(obj_pts=left_pts)
        lc = self.centre.projection_error(obj_pts=left_pts)
        lr = self.right.projection_error(obj_pts=left_pts)

        right_pts = self.triangulate_right().copy()
        rl = self.left.projection_error(obj_pts=right_pts)
        rc = self.centre.projection_error(obj_pts=right_pts)
        rr = self.right.projection_error(obj_pts=right_pts)

        residuals = np.concatenate([ll, lc, lr, rl, rc, rr])
        return residuals

    def _centre_sparsity(self):
        """
        Generate the sparse matrix, by marking as non zero the coincidence of
        each residual and the parameters involved in its calculation.
        Here, Every residual requires all the camera parameters, because we project
        from all three cameras. Then, each r/t (6 params) affects (target_size)
        residuals in each camera.
        """
        n_residuals = len(self.residuals())
        n_params = len(self.params())
        target_size = self.left.img_pts.shape[1]
        n = n_residuals // target_size
        A = lil_matrix((n_residuals, n_params), dtype=int)

        # fill the image parameters
        A[:, :27] = 1

        for i in range(n):
            A[i*target_size:i*target_size+target_size,
              24+i*6:24+6+i*6] = 1

        return A

    def _left_right_sparsity(self):
        """
        Generate the sparse matrix, by marking as non zero the coincidence of
        each residual and the parameters involved in its calculation.
        Here, Every residual requires all the camera parameters, because we project
        from all three cameras. Then, each r/t (6 params) affects (target_size)
        residuals in each camera.
        """
        n_residuals = len(self.residuals())
        n_params = len(self.params())
        target_size = self.left.img_pts.shape[1]
        n = (n_residuals // target_size) // 2
        k = n_residuals // 2
        A = lil_matrix((n_residuals, n_params), dtype=int)

        # fill the image parameters: left-centre, centre-right
        A[:k, :14] = 1
        A[k:, 10:24] = 1

        for i in range(n):
            A[i*target_size:i*target_size+target_size,
              24+i*6:24+6+i*6] = 1
            A[i*target_size + k:i*target_size+target_size + k,
              24+i*6:24+6+i*6] = 1
        return A

    def residuals(self):
        """Return the residual errors. For each camera, the euclidian
        re-projection error.
        """
        return self._centre_loss() if self.centre_loss \
                            else self._left_right_loss()

    def bundle_adjustment_sparsity(self):
        """
        Generate the sparse matrix, by marking as non zero the coincidence of
        each residual and the parameters involved in its calculation.
        Here, Every residual requires all the camera parameters, because we project
        from all three cameras. Then, each r/t (6 params) affects (target_size)
        residuals in each camera.
        """
        return self._centre_sparsity() if self.centre_loss \
                else self._left_right_sparsity()

    def fun(self, params):
        """ From a list of parmaeters, return the residuals. """
        self.load_params(params)
        return self.residuals()

# -----------------------------------------------------------------------------
# plotting
# -----------------------------------------------------------------------------

    def plot_cameras(self, ax, title='Camera Positions', equal=True,
                     label=True):
        for cam in self.cams:
            cam.plot_camera(ax)
        if equal:
            ax_equal(ax)
        if label:
            ax_label(ax)
        ax.set_title(title)

    def plot_proj(self, ax, title='Object Projections', equal=True,
                  label=True, world_transform=True):
        """Plot a projection of the observed targets for each camera."""
        cols = ['r', 'g', 'b']
        labels = ['left', 'centre', 'right']
        for cam, c, l in zip(self.cams, cols, labels):
            targets = cam.targets_to_world(reshape=True).T.copy()
            ax.scatter(*targets, c=c, marker='x', label=l)
        if equal:
            ax_equal(ax)
        if label:
            ax_label(ax)
        ax.legend()
        ax.set_title(title)

    def plot_stereo(self, ax, title='Stereo Projection', equal=True,
                    label=True):
        """Plot the triangulated 3d points."""
        prj_pts = self.triangulate_points().T
        ax.scatter(*zip(prj_pts), c=[0.1, 0.1, 0.1, 0.9], marker='x')
        if equal:
            ax_equal(ax)
        if label:
            ax_label(ax)
        ax.set_title(title)
