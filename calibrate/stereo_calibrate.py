""" module to calibrate stereo cameras. """
from __future__ import print_function
import numpy as np
import pickle
import cv2
import logging
import glob
import os
from zipfile import ZipFile

from .calibrate import BRD_SIZE, IMG_SIZE, SQR_SIZE, CRITERIA, CALIB_FLAGS
from .calibrate import calibrate_camera
from .utils import object_points, ChessboardNotFoundError, find_corners

logger = logging.getLogger(__name__)
STEREO_FLAGS = cv2.CALIB_FIX_INTRINSIC
N_OBS = 50  # number of observations to retain when recalibrating


# -----------------------------------------------------------------------------
# Image generators
# -----------------------------------------------------------------------------

def yield_from_zip(fname):
    """
    Yield a 3-tuple of openCV BGR images from a zip archive.
    The archive must contain jpg files named:
        cam_0_001.jpg, cam_1_001.jpg, cam_2_001.jpg ...

    """
    def _dec(x):
        return cv2.imdecode(np.frombuffer(x, np.uint8), 1)

    with ZipFile(fname, 'r') as zf:
        flist = zf.namelist()
        cam0 = [f for f in flist if 'cam_0' in f]
        cam1 = [f for f in flist if 'cam_1' in f]
        cam2 = [f for f in flist if 'cam_2' in f]
        for data in zip(cam0, cam1, cam2):
            yield [_dec(zf.read(x)) for x in data]


def yield_from_dir(dir_name):
    """
    Yield a 3-tuple of openCV BGR images from a directory.
    The directory must contain jpg files named:
        cam_0_001.jpg, cam_1_001.jpg, cam_2_001.jpg ...

    """
    def _dec(x):
        return cv2.imread(x)
    dname = os.path.join(dir_name, '')
    flist = glob.glob(dname + '*.jpg')
    cam0 = [f for f in flist if 'cam_0' in f]
    cam1 = [f for f in flist if 'cam_1' in f]
    cam2 = [f for f in flist if 'cam_2' in f]
    for data in zip(cam0, cam1, cam2):
        yield [_dec(x) for x in data]

# -----------------------------------------------------------------------------
# calibrate
# -----------------------------------------------------------------------------


def calibrate_cameras(img_gen, brd_size=BRD_SIZE, square_size=SQR_SIZE,
                      img_size=IMG_SIZE, flags=CALIB_FLAGS, criteria=CRITERIA):
    """
    Calibrate a number of cameras from a generator of opencv image tuples.
    If corners are found in all images of the tuple, then those corners are
    used for camera calibration. Length of tuple is number of cameras, and
    the number of successful corner detections is matched by 3d object
    points and saved with the resulting calibration.
    returns a list of calibration dictionaries.
    """

    op = object_points(brd_size[0], brd_size[1], square_size)
    img_pts, calibs = [], []
    # find corners in each image
    for i, images in enumerate(img_gen):
        try:
            ips = [find_corners(image, brd_size, criteria) for image in images]
        except ChessboardNotFoundError:
            continue
        img_pts.append(ips)
        logger.debug('appending {}th corners...'.format(i))
    n_found, n_cams = len(img_pts), len(images)
    logger.info('Image sets with found corners: {}'.format(n_found))
    logger.info('Number of cameras: {}'.format(n_cams))
    # calibrate each camera
    for i, ips in enumerate(zip(*img_pts)):
        calibs.append(calibrate_camera([op] * n_found, ips, img_size))
        logger.debug('Calibrating {}th camera'.format(i))
    return calibs


def calibrate_pair(left, right, flags=STEREO_FLAGS, criteria=CRITERIA):
    """ Calibrate a stereo pair of cameras.
    left and right are dictionaries of camera calibrations.
    """
    obj_pts = left['obj_pts']
    lft_pts = left['img_pts']
    rht_pts = right['img_pts']
    img_size = right['img_size']
    lm, ld = left['mtx'], left['dist']
    rm, rd = right['mtx'], right['dist']

    try:
        [rms, _, _, _, _, R, T, E, F] = cv2.stereoCalibrate(
            obj_pts, lft_pts, rht_pts, lm, ld, rm, rd,
            img_size, flags=flags, criteria=criteria)
    except Exception as e:
        raise e
    logger.info('Stereo RMS error: {}'.format(rms))
    return R, T, E, F


# -----------------------------------------------------------------------------
# recalibration - refining the first calibration
# -----------------------------------------------------------------------------


def reprojection_errors(calib):
    """
    Calculate the reprojection errors for the camera calibration.
    Returns an nd array of shape (n_observations, 2, rows*cols)
    """
    errors = []
    for rvec, tvec, ip, op in zip(calib['rvecs'], calib['tvecs'],
                                  calib['img_pts'], calib['obj_pts']):
        pp = cv2.projectPoints(op, rvec, tvec, calib['mtx'], calib['dist'])[0]
        errors.append((pp.squeeze() - ip.squeeze()).reshape(2, -1))
    return np.asfarray(errors)


def ranked_idx(calibs, limit=N_OBS):
    """ rank the errors and discard the worst """
    repro_errors = np.concatenate([reprojection_errors(c) for c in calibs], -1)
    errors = [(np.abs(e).max(), i) for i, e in enumerate(repro_errors)]
    idx = [i[1] for i in sorted(errors)[:limit]]
    return np.array(idx, dtype='int32')


def recalibrate(calibs, limit=N_OBS, flags=CALIB_FLAGS, criteria=CRITERIA):
    """ Using the best reprojection error indices, recalibrate the cameras """
    idx = ranked_idx(calibs, limit=limit)
    recalibs = []
    for c in calibs:
        recalibs.append(
            calibrate_camera(c['obj_pts'][idx],
                             c['img_pts'][idx],
                             c['img_size'],
                             flags, criteria))
    return recalibs
