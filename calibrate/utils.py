import cv2
import numpy as np
import logging
import glob
import os

logger = logging.getLogger(__name__)


class ChessboardNotFoundError(Exception):
    """No chessboard could be found in searched image."""


def yield_from_dir(dir_name, glob='jpg'):
    """Generator of openCV images from a directory of images."""
    dname = os.path.join(dir_name, '')
    flist = glob.glob(dname + glob)
    for f in flist:
        yield cv2.imread(f)


def object_points(rows, cols, square_size):
    points = np.zeros((rows*cols, 3), np.float32)
    points[:, :2] = np.mgrid[0:rows, 0:cols].T.reshape(-1, 2) * square_size
    return points


def find_corners(image, brd_size, criteria):
    """Find subpixel chessboard corners in openCV image."""
    grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret, corners = cv2.findChessboardCorners(grey, brd_size, None)
    if not ret:
        raise ChessboardNotFoundError('No chessboard could be found')
    corners = cv2.cornerSubPix(
        grey, corners, (11, 11), (-1, -1), criteria=criteria)

    # make some effort to have boards with rotational symmetry work.
    if corners[-1, 0, -1] < corners[0, 0, -1]:
        corners[:] = corners[::-1]
    return corners


def getM(R, t, inv=False):
    """
    Form transformation matrix from Rotation matrix and translation vector.
    If R is riation vector convert to Matrix.
    """
    if R.shape != (3, 3) and R.size == 3:
        R = rodrigues(R)
    M = np.eye(4)
    M[:3, :3] = R
    M[:3, 3] = t.ravel()
    if inv:
        return np.linalg.inv(M)
    return M


def getRt(M):
    R = M[:3, :3]
    t = M[:3, 3].reshape(3, 1)
    return R, t


def invRt(R, t):
    return getRt(getM(R, t, inv=True))


def rodrigues(r):
    return np.asarray(cv2.Rodrigues(np.asfarray(r))[0])


def homogenise(X):
    """ Convert NxM to NxM+1"""
    return np.hstack([X, np.ones([X.shape[0], 1])]).astype('float32')


def dehomogenise(X):
    """ Convert NxM+1 to NxM"""
    return X[:, :-1].astype('float32')


def solvePnP(obj_pts, img_pts, mtx, dist):
    """ Wrap cv2.solvePnP and return 4x4 transform matrix."""
    ret, r, t = cv2.solvePnP(obj_pts, img_pts, mtx, dist)
    logger.debug('Return value: {}'.format(ret))
    return get_transform(r, t)


def get_transform(r, t, inv=False):
    """
    From r, t form the 4x4 transfomation matrix.
    r and t are lists, tuples or arrays of length 3.
    If inv == True, return the inverse matrix.
    """
    M = np.eye(4)
    M[:3, :3] = rodrigues(r)
    M[:3, 3] = np.asfarray(t).ravel()
    return np.linalg.inv(M) if inv else M


def projection_matrix(mtx, R=None, t=None):
    """
    Form the projection matrix:
    P = K * [R|t]
    If R is None, then we use identity matrix.
    """
    if R is None:
        R = np.eye(3)
    if t is None:
        t = np.zeros([3, 1])
    Rt = np.hstack([R, t.reshape([3, 1])])
    return np.dot(mtx, Rt).astype('float32')


def procrustes(X, Y):
    """
    A modified port of MATLAB's `procrustes` function to Numpy.
        d, Z, [tform] = procrustes(X, Y)
    NB. This does not return the same value for t as MATLAB.

    Inputs:
    ------------
    X, Y
        matrices of target and input coordinates. they must have equal
        numbers of  points(rows), but Y may have fewer dimensions
        (columns) than X.

    Outputs
    ------------
    d: the residual sum of squared errors, normalized according to a
        measure of the scale of X, ((X - X.mean(0))**2).sum()

    Z: the matrix of transformed Y-values

    R, t: The rotation, translation that maps X --> Y,
    such that R*X+t = Y (approx)
    to match the behavior of openCV stereocalibrate.
    """

    muX = X.mean(0)
    muY = Y.mean(0)

    X0 = X - muX
    Y0 = Y - muY

    ssX = (X0**2.).sum()
    ssY = (Y0**2.).sum()

    # centred Frobenius norm
    normX = np.sqrt(ssX)
    normY = np.sqrt(ssY)

    # scale to equal (unit) norm
    X0 /= normX
    Y0 /= normY

    # optimum rotation matrix of Y
    A = np.dot(X0.T, Y0)
    U, s, Vt = np.linalg.svd(A, full_matrices=False)
    R = np.dot(Vt.T, U.T)

    # residuals
    d = 1 + ssY/ssX - 2 * s.sum() * normY / normX

    # transform Y
    Z = normY*np.dot(Y0, R) + muX

    # translation matrix
    t = muY - np.dot(R, muX.T)

    return d, Z, R, t

# -----------------------------------------------------------------------------
# Distortion
# -----------------------------------------------------------------------------

def undistort_points(img_pts, K, dist):
    """
    Return undistorted image points.
    img_pts: N*2 array of points in image coordinates.
    K: camera matrix
    dist: distortion coefficients.
    return N*2 array of undistorted points.
    """
    img_pts = np.asfarray(img_pts)
    if not (len(img_pts.shape) == 2 and img_pts.shape[1] == 2):
        raise ValueError(
            'input should be N*2 array, got shape {}'.format(img_pts.shape))
    u, v = img_pts.T
    k1, k2, p1, p2, k3 = dist
    u0 = K[0, 2]  # cx
    v0 = K[1, 2]  # cy
    fx = K[0, 0]
    fy = K[1, 1]

    y = (v - v0)/fy
    x = (u - u0)/fx
    r2 = x**2 + y**2

    u_undistort = (x * (1 + (k1*r2) + (k2*r2**2) + (k3*r2**3))) + \
        2*p1*x*y + p2*(r2 + 2*x**2)
    v_undistort = (y * (1 + (k1*r2) + (k2*r2**2) + (k3*r2**3))) + \
        2*p2*y*x + p1*(r2 + 2*y**2)
    # cv2 returns here
    x_undistort = fx*u_undistort + u0
    y_undistort = fy*v_undistort + v0
    return np.vstack([x_undistort, y_undistort]).T


# -----------------------------------------------------------------------------
# Triangulation
# -----------------------------------------------------------------------------


def triangulate_nviews(P, ip):
    """
    Triangulate a point visible in n camera views.
    P is a list of camera projection matrices.
    ip is a list of homogenised image points. eg [ [x, y, 1], [x, y, 1] ], OR,
    ip is a 2d array - shape nx3 - [ [x, y, 1], [x, y, 1] ]
    len of ip must be the same as len of P
    Return projected point.
    """
    if not len(ip) == len(P):
        raise ValueError('Number of points and number of cameras not equal.')
    n = len(P)
    M = np.zeros([3*n, 4+n])
    for i, (x, p) in enumerate(zip(ip, P)):
        M[3*i:3*i+3, :4] = p
        M[3*i:3*i+3, 4+i] = -x
    V = np.linalg.svd(M)[-1]
    X = V[-1, :4]
    return X


def triangulate_points(P1, P2, x1, x2):
    """
    Two-view triangulation of points in
    x1,x2 (nx3 homog. coordinates).
    Similar to openCV triangulatePoints.
    """
    if not len(x2) == len(x1):
        raise ValueError("Number of points don't match.")
    X = np.array([triangulate_nviews([P1, P2], [x[0], x[1]])
                  for x in zip(x1, x2)])
    return X/X[:, 3].reshape(-1, 1)


def triangulate_npoints(P, ip):
    """
    Two-view triangulation of points in
    x1,x2 (nx3 homog. coordinates).
    Similar to openCV triangulatePoints.
    """
    if not len(P) == len(ip):
        raise ValueError("Number of points don't match.")
    X = np.array([triangulate_nviews(P, x) for x in zip(*ip)])
    return X/X[:, 3].reshape(-1, 1)
