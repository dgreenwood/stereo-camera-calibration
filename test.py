from __future__ import print_function
import glob
import pickle
import numpy as np
import cv2
from zipfile import ZipFile
import logging
import os
import matplotlib.pyplot as plt
import requests

import calibrate.calibrate as calib
import calibrate.stereo_calibrate as scal
import calibrate.bundle as bnd
import calibrate.plot as plot

# -----------------------------------------------------------------------------
# test data
# -----------------------------------------------------------------------------

# test data
url = 'https://www.dropbox.com/s/dl/p3hxjuyrrcttpn0/calibrate-test-data.zip'
save_name = 'test-data.zip'

if not os.path.exists('temp'):
    os.makedirs('temp')

# -----------------------------------------------------------------------------
# logging
# -----------------------------------------------------------------------------

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
logger.info('Starting tests:')

# -----------------------------------------------------------------------------
# helpers
# -----------------------------------------------------------------------------


def get_test_data(url, save_name):
    if os.path.isfile(save_name):
        logger.info('Test data exists, skipping....')
        return
    r = requests.get(url, stream=True)
    with open(save_name, 'wb') as fid:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                fid.write(chunk)
    logger.info('Test data downloaded....')


def calibs_equal(d1, d2):
    """ return True if all keys and values are the same """
    result = True
    for k in d1:
        if isinstance(d1[k], np.ndarray):
            result = np.array_equal(d1[k], d2[k])
        else:
            result = d1[k] == d2[k]
    return result


def test_corners(*args):
    try:
        corners = calib.find_corners(args[0])
        return corners
    except Exception as e:
        assert isinstance(e, calib.ChessboardNotFoundError)
        logger.debug('test1 {}'.format(e))
        return None

# -----------------------------------------------------------------------------
# download the test data
# -----------------------------------------------------------------------------

get_test_data(url, save_name)

# -----------------------------------------------------------------------------
# test object_points
# -----------------------------------------------------------------------------

rows, cols, size = 5, 9, 0.015
obj_pts = calib.object_points((rows, cols), size)

assert obj_pts.shape == (rows*cols, 3)
assert np.isclose(obj_pts[1, 0], size)

# -----------------------------------------------------------------------------
# test find_corners
# -----------------------------------------------------------------------------

with ZipFile(save_name, 'r') as zf:
    cam0 = [zf.read(f) for f in zf.namelist() if 'cam_0' in f]
    cam1 = [zf.read(f) for f in zf.namelist() if 'cam_1' in f]
    cam2 = [zf.read(f) for f in zf.namelist() if 'cam_2' in f]

img = cv2.imdecode(np.frombuffer(cam0[0], np.uint8), 1)
brd_size = (3, 7)
corners = test_corners(img, brd_size)
assert corners.shape[0] == brd_size[0] * brd_size[1]

brd_size = (3, 7)
op = calib.object_points(brd_size, 0.02)
ops, ips = [], []
for i, blob in enumerate(cam0):
    img = cv2.imdecode(np.frombuffer(blob, np.uint8), 1)
    try:
        crn = calib.find_corners(img, brd_size)
    except:
        continue
    ips.append(crn)
    ops.append(op)
assert len(ops) == len(ips)
logger.info('Done find_corners:')

# -----------------------------------------------------------------------------
# test yield from zip
# -----------------------------------------------------------------------------

gen = scal.yield_from_zip(save_name)
images = next(gen)
for image in images:
    assert isinstance(image, np.ndarray)
logger.info('Done yield_from_zip:')

# -----------------------------------------------------------------------------
# test calibrate
# -----------------------------------------------------------------------------

c1 = calib.calibrate_camera(ops, ips)
logger.info(c1.keys())

c2 = calib.calibrate_camera(np.array(ops, dtype='float32').squeeze(),
                            np.array(ips, dtype='float32').squeeze())
logger.info(c2.keys())
logger.info('Done calibrate_camera:')

# -----------------------------------------------------------------------------
# test load and save
# -----------------------------------------------------------------------------

fname = 'temp/test.pkl'
calib.save(c1, fname)
c3 = calib.load(fname)
assert calibs_equal(c1, c3)
logger.info('Done load and save:')

# -----------------------------------------------------------------------------
# test min_calib + get_intrinsics
# -----------------------------------------------------------------------------

mini_cal = calib.min_calib(c1)
assert mini_cal[0] == c1['mtx'][0, 0]
assert mini_cal[1] == c1['dist'][0]
assert mini_cal[2] == c1['dist'][1]
assert mini_cal[3] == c1['dist'][-1]

M, d = calib.get_intrinsics(mini_cal)
assert np.allclose(M, c1['mtx'])
assert np.allclose(d, c1['dist'])
logger.info('Done min_calib:')

# -----------------------------------------------------------------------------
# test reprojection_errors
# -----------------------------------------------------------------------------

errors = calib.reprojection_errors(c1)
assert isinstance(errors, np.ndarray)
assert errors.shape[1] == 2
assert errors.shape[-1] == c1['img_pts'].shape[1]

fig, ax = plt.subplots(1)
plot.repro_error(ax, c1)
fig.savefig('temp/repro.png')
logger.info('Done reprojection_errors:')

# -----------------------------------------------------------------------------
# test ranked_idx
# -----------------------------------------------------------------------------

lim = 10
idx = scal.ranked_idx([c1, c2, c3], lim)
logger.info(idx)
assert len(idx) == lim
logger.info('Done ranked_idx:')

# -----------------------------------------------------------------------------
# test calibrate_cameras
# -----------------------------------------------------------------------------

calibs = scal.calibrate_cameras(scal.yield_from_zip(save_name))
for i, c in enumerate(calibs):
    fig, ax = plt.subplots(1)
    plot.repro_error(ax, c)
    fig.savefig('temp/{}.png'.format(i))
    assert isinstance(c, dict)
logger.info('Done calibrate_cameras:')

# -----------------------------------------------------------------------------
# test recalibrate
# -----------------------------------------------------------------------------

recalibs = scal.recalibrate(calibs, limit=5)
for i, c in enumerate(recalibs):
    fig, ax = plt.subplots(1)
    plot.repro_error(ax, c)
    fig.savefig('temp/re_{}.png'.format(i))
    assert isinstance(c, dict)
logger.info('Done recalibrate:')

# -----------------------------------------------------------------------------
# test calibrate pair
# -----------------------------------------------------------------------------

R, T, E, F = scal.calibrate_pair(calibs[0], calibs[1])
assert R.shape == (3, 3)
assert T.shape == (3, 1)
assert E.shape == (3, 3)
assert F.shape == (3, 3)
logger.info('Done calibrate_pair:')

# -----------------------------------------------------------------------------
# test calibrate_pairs
# -----------------------------------------------------------------------------

ret = bnd.calibrate_pairs(calibs)
n, k = len(calibs), len(ret)
assert k == 4 * n + 6 * (n - 1)
assert ret[0] == calibs[0]['mtx'][0, 0]
logger.info(ret)
logger.info('Done calibrate_pairs:')

# -----------------------------------------------------------------------------
# test get_transform
# -----------------------------------------------------------------------------

r, t = ret[4:7], ret[7:10]
M = bnd.get_transform(r, t)
Mi = bnd.get_transform(r, t, inv=True)
assert np.allclose(Mi, np.linalg.inv(M))
logger.info(M)
logger.info(Mi)
logger.info('Done get_transform:')

# -----------------------------------------------------------------------------
# test solvePnP
# -----------------------------------------------------------------------------

op, ip = calibs[0]['obj_pts'][0], calibs[0]['img_pts'][0]
m, d = calibs[0]['mtx'], calibs[0]['dist']
M = bnd.solvePnP(op, ip, m, d)
logger.info(M)
logger.info('Done solvePnP:')

# -----------------------------------------------------------------------------
# test compute_distance
# -----------------------------------------------------------------------------

left, right, r, t = ret[:4], ret[10:14], ret[4:7], ret[7:10]
ops, lps, rps = calibs[0]['obj_pts'], calibs[0]['img_pts'], calibs[1]['img_pts']

M = bnd.get_transform(r, t)
cd = bnd.compute_distance(right, left, M, ops, rps, lps)

Mi = bnd.get_transform(r, t, inv=True)
cdi = bnd.compute_distance(left, right, Mi, ops, lps, rps)

assert np.allclose(np.array(cdi), np.array(cd))
logger.info('Done compute_distance:')

# -----------------------------------------------------------------------------
# test initial values
# -----------------------------------------------------------------------------

x0, obj_pts, img_pts = bnd.get_initial_values(calibs)
logger.info('x0 shape {}'.format(x0.shape))
logger.info('Done get_initial_values:')

# -----------------------------------------------------------------------------
# test ranked_idx (bundle)
# -----------------------------------------------------------------------------

residuals = bnd.fun(x0, obj_pts, img_pts)
shape = [len(calibs), len(calibs[0]['obj_pts']), -1]
idx = bnd.ranked_idx(residuals, shape)
logger.info('Shape shape {}'.format(shape))
logger.info('Residuals shape {}'.format(residuals.shape))
logger.info('IDX shape {}'.format(idx.shape))
logger.info('Done ranked_idx:')

# -----------------------------------------------------------------------------
# test optimise
# -----------------------------------------------------------------------------

xn = bnd.optimise(calibs, iters=4)
logger.info(xn)

# -----------------------------------------------------------------------------
# Finished
# -----------------------------------------------------------------------------

logger.info('Finished!')
